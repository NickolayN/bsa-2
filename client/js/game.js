import Timer from './services/timer.js';
import GameView from './views/gameView.js';
import {
  countSpeed,
  checkResult
} from "./services/services.js";

class Game {
  constructor() {
    this.state = {
      time: 0,
      progress: 0,
      mistakes: 0,
      players: new Map()
    }

    this.tick = this.tick.bind(this);
    this.clickHandler = this.clickHandler.bind(this);
  }

  set time(time) {
    this.state = {
      ...this.state,
      time
    };
  }

  get time() {
    return this.state.time;
  }

  set progress(progress) {
    this.state = {
      ...this.state,
      progress
    };
  }

  get progress() {
    return this.state.progress;
  }

  set mistakes(mistakes) {
    this.state = {
      ...this.state,
      mistakes
    };
  }

  get mistakes() {
    return this.state.mistakes;
  }

  get players() {
    return this.state.players;
  }

  set players(players) {
    this.state = {
      ...this.state,
      players
    }
  }

  updatePlayer(player) {
    this.state.players.delete(player.user);
    this.state.players.set(player.user, {progress: player.progress});

    GameView.renderPlayers(this.players, this.text.length);
  }

  start() {
    GameView.renderScreen();
    this.timer = new Timer(this.tick).start();

    GameView.renderTime(this.time);
    GameView.renderText(this.text);
    GameView.renderSpeed(countSpeed(this.time, this.progress));
    GameView.renderMistakes(this.mistakes);
    GameView.renderPlayers(this.players, this.text.length);

    document.addEventListener('keyup', this.clickHandler);
  }

  stop() {
    this.timer.stop();
  }

  tick(time) {
    this.time = time;
    GameView.renderTime(this.time);

    const speed = countSpeed(this.time, this.progress);
    GameView.renderSpeed(speed);
  }

  onProgress() {

  }

  clickHandler(evt) {
    const result = checkResult(this.text, gameInput.value);
    if (result.done) {
      console.log("Finish");
    } else {
      console.log(result);
    }

    if (result.mistake >= 0) {
      this.mistakes = this.mistakes + 1;
      GameView.renderMistakes(this.mistakes);
    }

    if (result.progress >= 0) {
      this.progress = result.progress;
      this.onProgress(this.progress);
    }
    GameView.renderText(this.text, result);
    const speed = countSpeed(this.time, this.progress);
    GameView.renderSpeed(speed);
  }
}

export default Game;