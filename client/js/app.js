import {
  gameService
} from "./services/gameService.js";
import Game from './game.js';

import timeToNextRace from './views/timeToNextRaceView.js';

class App {
  constructor() {
    this.initGame();
  }

  async startGame() {    
    this.game.start();
  }

  initGame() {
    const jwt = localStorage.getItem('jwt');
    const socket = io.connect(`http://localhost:3000?token=${jwt}`);

    this.game = new Game();
    this.game.onProgress = (progress) => {
      socket.emit('progress', progress);
    }

    socket.on('joinRace', async ({ time, textId }) => {
      App.showTimeToStart(time);

      try {
        this.game.text = await gameService.getText(textId);
      } catch (error) {
        console.log(error);
      }      
    });

    socket.on('startGame', () => this.startGame());

    socket.on('playerJoined', (payload) => {
      this.game.players = new Map(payload);
    });

    socket.on('progress', payload => {
      this.game.updatePlayer(payload);
    });
  }

  static showTimeToStart(time) {
    const seconds = Math.floor((time - Date.now()) / 1e3);
    root.innerHMTL = '';
    root.append(timeToNextRace(seconds));
  }
}

new App();