import { callApi } from "../helpers/apiHelper.js";

class GameService {
  async getText(id) {
    const endpoint = `text/${id}`;

    try {
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const gameService = new GameService();