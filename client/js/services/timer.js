class Timer {
  constructor(callback) {
    this.callback = callback;
    this.time = 0;
  }

  start() {
    this.callback(0);

    this.timerId = setInterval(() => {
      this.tick();
    }, 1000);
  }

  stop() {
    clearInterval(this.timerId);
  }

  tick() {
    this.time = this.time + 1;
    this.callback(this.time);
  }
}

export default Timer;