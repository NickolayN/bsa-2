async function login(user) {
  fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  }).then(res => {
    res.json().then(body => {
      if (body.auth) {
        localStorage.setItem('jwt', body.token);
        location.replace('/game.html');
      } else {
        console.log('auth failed');
      }
    })
  }).catch(err => {
    console.log('request went wrong');
  });
}

export { login };