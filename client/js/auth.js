window.onload = () => {

  const jwt = localStorage.getItem('jwt');

  if (jwt) {
    location.replace('/game.html');
  } else {
    location.replace('/login.html');
  }
  
}