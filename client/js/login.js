import { login } from "./services/authSevice.js";

const form = document.forms['login-form'];


function submitHandler(event) {
  event.preventDefault();

  const userLogin = form.login.value;
  const password = form.password.value;
  
  login({login: userLogin, password});
}

form.onsubmit = submitHandler;