function timeToNextRace(time) {
  const elem = document.createElement('div');
  elem.innerText = 'The next race starts in: '
  const timer = document.createElement('span');
  elem.appendChild(timer);
  timer.setAttribute('id', 'timer');
  timer.innerText = time;
  const timerId = setInterval(() => {
    time--;
    timer.innerText = time;
    if (time <= 0) clearInterval(timerId);    
  }, 1000);

  return elem;
}

export default timeToNextRace;