class GameView {
  static renderScreen() {
    const template = `<div class="game-header">
    <ul class="game-menu">
      <li class="menu-item time" id="gameTime">30</li>
      <li class="menu-item" id="gameSpeed">65/min</li>
      <li class="menu-item" id="GameMistakes">2</li>
    </ul>
  </div>
  <div class="game-block" id="gameBlock">
    <div class="game-text" id="gameText">Waiting...</div>
    <textarea class="game-input" id="gameInput" cols="30" rows="10"></textarea>
  </div>
  <ul class="users-list" id="usersList">
  </ul>`;

    root.innerHTML = template;
  }

  static renderPlayers([...players], length) {
    usersList.innerHTML = '';
    
    players.map(player => {
      const li = document.createElement('li');
      li.classList.add('players-progress');
      li.innerText = player[0];

      const playerElem = document.createElement('progress');
      playerElem.max = length;
      playerElem.value = player[1].progress;
      li.append(playerElem);

      usersList.append(li);
    });
  }

  static renderTime(time) {
    gameTime.innerHTML = time;
  }

  static renderSpeed(speed) {
    gameSpeed.innerHTML = speed;
  }

  static renderMistakes(mistakes) {
    GameMistakes.innerHTML = mistakes;
  }

  static renderText(text, {
    progress,
    mistake
  } = {}) {
    let editedText = '';

    if (progress) {
      editedText += `<span class="right">${text.slice(0, progress)}</span>`;
    }

    if (mistake) {
      editedText += `<span class="wrong">${text.slice(progress, mistake)}</span>`;
    }

    editedText += text.slice(mistake || progress);

    gameText.innerHTML = editedText;
  }
}

export default GameView;