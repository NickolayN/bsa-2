function renderStatistic(players) {
  const ul = document.createElement('ul');
  const playersStr = players.map(player => `<li>${player.login}</li>`);
  ul.innerHTML = playersStr;

  return ul;
}