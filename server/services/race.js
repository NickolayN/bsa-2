const TIME_TO_START = 3e4;

// ---change it---
const TEXTS = 3;
// -----------

class Race {
  constructor(id) {
    this.id = id;
    this.startTime = Date.now() + TIME_TO_START;    
    this.textId = this.getTextId();
    this.players = new Map();
    this.playersCount = 0;
    this.isActive = false;
  }

  getPlayer(id) {
    return this.players.get(id);
  }

  addPlayer(player) {
    this.playersCount++;
    this.players.set(player, {progress: 0});
  }

  deletePlayer() {
    this.playersCount--;
  }

  updatePlayer(player, progress) {
    this.players.delete(player);
    this.players.set(player, {progress});
  }

  isAlive() {
    return !!this.playersCount;
  }

  getTextId() {
    return this.getRandom(0, TEXTS);
  }

  getRandom(min, max) {
    const rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  }
}

module.exports = Race;