const passport = require('passport');
const {getUserFromSocket} = require('../helpers/socket.helper');

require('../passport.config');

function authMiddleware() {
  return passport.authenticate('jwt', {session: false});
}

function socketMiddleWare(socket, next) {
  try {
    getUserFromSocket(socket);
    return next();
  } catch (error) {
    socket.disconnect();
  }  
}

module.exports = {authMiddleware, socketMiddleWare};