const path = require('path');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
// -------------auth----------------
const passport = require('passport');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

require('./passport.config.js');
app.use(passport.initialize());
app.use(bodyParser.json());


const indexRouter = require('./routes/index');
const textRouter = require('./routes/text');
const loginRouter = require('./routes/login');

app.use(express.static(path.join(__dirname, '../client/')));
app.use('/', indexRouter);
app.use('/text', textRouter);
app.use('/login', loginRouter);


const {socketMiddleWare} = require('./middlewares/auth.middleware');
const {getUserFromSocket} = require('./helpers/socket.helper');
const Race = require('./services/race');


let race = null;
const TIME_TO_START = 3e4;

io.use(socketMiddleWare);

io.on('connection', (socket) => {
  const user = getUserFromSocket(socket);

  if (!race) {
    race = new Race(user.login);
  } 

  socket.join(race.id);

  io.emit('joinRace', {
    time: race.startTime,
    textId: race.textId,
    players: race.players
  });

  race.addPlayer(user.login);
  io.to(race.id).emit('playerJoined', [...race.players]);

  setTimeout(() => {
    io.emit('startGame');
    race.isActive = true;
  }, TIME_TO_START);

  socket.on('progress', payload => {
    const user = getUserFromSocket(socket).login;
    race.updatePlayer(user, payload);

    io.to(race.id).emit('progress', {user, progress: payload});
  });

   
  socket.on('disconnect', () => {
    race.deletePlayer();
    console.log('someone disconnect');

    if (!race.isAlive) {      
      race = null;
    }
  });
});

// ---------------------
server.listen(3000);