const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../middlewares/auth.middleware');

const { getText } = require("../repositories/text.repository.js");

router.get('/:id', authMiddleware(), (req, res, next) => {
  const id = req.params.id;
  getText(id)
    .then(text => res.json(text));
});

module.exports = router;